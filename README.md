# BatLogger GUI
A interface gráfica em Java do BatLogger

![BatLogger GUI](https://raw.githubusercontent.com/cledsupper/batlogger-gui/master/img/BatLogger%20-%20Monitor.png "Aba principal")

Este é o código fonte da interface gráfica do BatLogger, caso você queira fazer clonar/baixar e fazer suas modificações :)

Sim, o BatLogger é parecido com o aplicativo BatteryBot para Android e inclusive seu design é inspirado nele, mas foi escrito e desenhado totalmente do 0.

by Ledso
