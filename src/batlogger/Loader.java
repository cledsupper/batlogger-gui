package batlogger;

/** É autoexplicativo, inicia o programa
 *
 * @author Cledson Ferreira
 */
public class Loader {
    public static void main(String[] args) {
        BLAutoSet.main(args);
        java.io.File runningFile = new java.io.File(VSet.getConfigFolder()+".running");
        if (runningFile.exists()) {
            javax.swing.JOptionPane.showMessageDialog(null, "O aplicativo já está em execução!", "OPS", javax.swing.JOptionPane.ERROR_MESSAGE);
            System.out.println("Se o aplicativo não está mesmo em execução, apague o arquivo .running do diretório " + runningFile.getParent());
            return;
        }
        try {
            runningFile.createNewFile();
            runningFile.deleteOnExit();
        } catch (java.io.IOException e) {
            System.err.print("Arquivo de execução não pôde ser criado!");
        }
        if (FSet.isInTrial()) {
            gui.BLHome.main(args);
        } else gui.BLMain.main(args);
    }
}