package batlogger;

/** Funções para o gerenciamento de eventos da bateria.
 *
 * @author Cledson Ferreira
 */
public class FSet {
    public static final boolean FULL_VERSION = true;
    private static String lastStatus = VSet.STATUS_STOPPED;

    // tenta obter o percentual preciso (ou não)
    public static double getPercent() {
        double nivel;
        double capacidade;

        if (VSet.levelFileUsing.equals(VSet.CHARGE_NOW_FILENAME)) {
            nivel = getCharge();
            //System.out.println("Nível = " + nivel);
            if (nivel == -1) return -1;
            capacidade = getChargeFull();
            //System.out.println("Capacidade = " + capacidade);
            if (capacidade == -1) return -1;
            nivel = nivel*100/capacidade;
            //System.out.println("Nível = " + nivel);
        } else {
            java.io.File arqNivel = new java.io.File(VSet.supplyFolder + VSet.levelFileUsing);
            try (java.util.Scanner sc = new java.util.Scanner(arqNivel)) {
                nivel = sc.nextInt();
            } catch (java.io.FileNotFoundException e) {
                nivel = -1.0;
            }
        }
        return nivel;
    }

    public static int getCharge() {
        java.io.File arqNivel = new java.io.File(VSet.supplyFolder + VSet.CHARGE_NOW_FILENAME);

        int nivel = -1;
        try (java.util.Scanner sc = new java.util.Scanner(arqNivel)) {
                nivel = sc.nextInt()/1000;
        } catch (java.io.FileNotFoundException e) {}
        return nivel;
    }
    
    public static int getChargeFull() {
        java.io.File arqNivel = new java.io.File(VSet.supplyFolder + VSet.CHARGE_FULL_FILENAME);

        int capacidade = -1;
        try (java.util.Scanner sc = new java.util.Scanner(arqNivel)) {
                capacidade = sc.nextInt()/1000;
        } catch (java.io.FileNotFoundException e) {}
        return capacidade;
    }

    public static String getStatus() {
        java.io.File statusFile = new java.io.File(VSet.supplyFolder + VSet.STATUS_FILENAME);
        String status;
        try (java.util.Scanner sc = new java.util.Scanner(statusFile)) {
            status = sc.next().toLowerCase();
        } catch (java.io.FileNotFoundException e) {
            status = VSet.STATUS_DISCHARGING;
        }
        switch (status) {
            case "discharging":
                status = VSet.STATUS_DISCHARGING;
                break;
            case "charging":
                status = VSet.STATUS_CHARGING;
                break;
            case "full":
                status = VSet.STATUS_FULL;
                break;
            default:
                int current = getCurrent();
                if (current==0) status = VSet.STATUS_STOPPED;
                else status = lastStatus;
        }
        lastStatus=status;
        return status;
    }
    
    public static String translateStatus(String status) {
        status = status.toLowerCase();
        switch (status) {
            case "discharging":
                status = VSet.STATUS_DISCHARGING;
                break;
            case "charging":
                status = VSet.STATUS_CHARGING;
                break;
            case "full":
                status = VSet.STATUS_FULL;
                break;
            default:
                status = VSet.STATUS_STOPPED; // provavelmente é isso
        }
        return status;
    }
    
    public static double getVoltage() {
        java.io.File voltageFile = new java.io.File(VSet.supplyFolder + VSet.VOLTAGE_FILENAME);
        double voltage = -1.0;
        try (java.util.Scanner sc = new java.util.Scanner(voltageFile)) {
            voltage = sc.nextInt()/1000000.0;
        } catch(java.io.FileNotFoundException e) {}
        return voltage;
    }

    public static String getHealth() {
        double capacity = getCapacity();
        if (capacity>85.0) {
            return VSet.HEALTH_GOOD;
        } else if (capacity>50) {
            return VSet.HEALTH_OLD;
        } else if (capacity>0) {
            return VSet.HEALTH_DYING;
        } else
            return VSet.HEALTH_DEAD;
    }

    public static double getCapacity() {
        double charge_full = getChargeFull();
        double charge_full_design;
        double capacity = -1.0;
        java.io.File chargeFullDesignFile = new java.io.File(VSet.supplyFolder + VSet.FULL_DESIGN_FILENAME);
        try (java.util.Scanner sc = new java.util.Scanner(chargeFullDesignFile)) {
            charge_full_design = sc.nextInt()/1000;
            capacity = charge_full*100/charge_full_design;
        } catch (java.io.FileNotFoundException e) {}
        return capacity;
    }
    
    public static int getCurrent() {
        int current = -1;
        java.io.File currentFile = new java.io.File(VSet.supplyFolder + VSet.CURRENT_FILENAME);
        try (java.util.Scanner sc = new java.util.Scanner(currentFile)) {
            current = sc.nextInt()/1000;
        } catch (java.io.FileNotFoundException e) {}
        return current;
    }

    static boolean isInTrial() {
        boolean test = FULL_VERSION;
        int timeNow = (int) (System.currentTimeMillis()/60000);
        int difference = timeNow - VSet.firstTime;
        System.out.println(difference);
        if (difference>=0 && difference <= VSet.TRIAL_DAYS_LIMIT)
            test = true;
        return test;
    }
 }
