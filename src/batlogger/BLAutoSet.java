package batlogger;

/**
 * Configura as variáveis da classe VSet com auxílio do usuário
 *
 * @author Cledson Ferreira
 */
public class BLAutoSet {

    public static void main(String[] args) {
        System.out.println("Carregando configuração do BatLogger...");
        String userhome = System.getProperty("user.home");
        VSet.configFolder = VSet.configFolder.replace("USERHOME", userhome);
        VSet.cacheFolder = VSet.cacheFolder.replace("USERHOME", userhome);
        VSet.appFolder = VSet.appFolder.replace("USERHOME", userhome);
        java.io.File configuracao = new java.io.File(
                VSet.configFolder+VSet.CONFIG_FILENAME);
        loadConfig(configuracao);
    }

    /**
     * doConfig() gera o arquivo de configuração do BatLogger Todas as
     * informações são salvas nas variáveis da classe VSet
     */
    private static void makeConfig() {
        System.out.println("Procurando arquivos gerenciados pelo sistema"
                + " operacional");
        if (!foundBatDir()) {
            System.out.println("ERRO #1: DIRETÓRIO BÁSICO NÃO ENCONTRADO!");
            javax.swing.JOptionPane.showMessageDialog(null, "Não encontrei uma bateria"
                    + " no seu dispositivo", "ERRO", javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(VSet.ERRO_1_MAINDIRNOTFOUND);
        }
        //System.out.println("Diretório básico: " + VSet.supplyFolder);

        if (!foundCapacity()) {
            System.out.println("ERRO #2: ARQUIVO PRINCIPAL NÃO ENCONTRADO!");
            javax.swing.JOptionPane.showMessageDialog(null, "Existem alguns arquivos"
                    + " de sistema que eu preciso utilizar. Um deles é o \"capacity\""
                    + " que não está presente no seu computador", "ERRO",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(VSet.ERRO_2_MAINFILENOTFOUND);
        }
        //System.out.println("Arquivo básico: " + VSet.supplyFolder + VSet.levelFileUsing);

        if (!saveSettingsFile()) {
            System.out.println("ERRO #3: falha ao salvar arquivo de configuração!"
                    + "\nVerifique se você tem permissões para modificar arquivos em: "
                    + VSet.configFolder);
            javax.swing.JOptionPane.showMessageDialog(null, "Eu não consegui salvar"
                    + " as configurações em " + VSet.configFolder + ", verifique"
                            + " se o endereço é correto e se você e eu temos permissões"
                            + " de escrita neste diretório, inclusive nos arquivos"
                            + " presentes nele", "ERRO",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(VSet.ERRO_3_OUTPUTERROR);
        }
        System.out.println("OK");
    }

    /**
     * loadConfig() carrega o arquivo de configuração do BatLogger Todas as
     * informações são salvas nas variáveis da classe VSet
     */
    private static void loadConfig(java.io.File configFile) {
        java.util.Scanner sc;
        try {
            sc = new java.util.Scanner(configFile);
            if (sc.hasNextLine()) {
                VSet.supplyFolder = sc.nextLine();
            } else {
                sc.close();
                makeConfig();
            }
            if (sc.hasNextLine()) {
                VSet.levelFileUsing = sc.nextLine();
            } else {
                sc.close();
                makeConfig();
            }
            if (sc.hasNextLine()) {
                VSet.firstTime = sc.nextInt();
            }
            /*System.out.println(" -- INFORMAÇÕES CARREGADAS --");
            System.out.println("Diretório de bateria: " + VSet.supplyFolder);
            System.out.println("Arquivo indicador de nível de energia: " + VSet.levelFileUsing);*/
            System.out.println("OK");
            sc.close();
        } catch (java.io.FileNotFoundException ex) {
            makeConfig();
        }
    }

    /**
     * foundBatDir() encontra no sistema o diretório de eventos da bateria
     */
    private static boolean foundBatDir() {
        java.util.Scanner sc = new java.util.Scanner(System.in);
        String entrada;
        boolean encontrado = false;

        /* 1.1 Procura do diretório */
        java.io.File folderFile = new java.io.File(VSet.supplyFolder);
        if (folderFile.exists()) {
            for (String file : folderFile.list()) {
                if (file.toLowerCase().contains("bat")) {
                    /* System.out.println("Encontrado: " + file
                        + ". Confirma? \"SIM\" ou \"nao\" (sem aspas):");
                    entrada = sc.next().toLowerCase();
                    if (entrada.equals("nao") || entrada.equals("no"))
                        continue; */
                    VSet.supplyFolder += file + java.io.File.separator;
                    encontrado = true;
                    break;
                }
            }
            sc.close();
        } else {
            System.out.println("ERRO: você não deve estar usando o Linux, o seu"
                    + " sistema operacional ainda não é suportado :/");
            sc.close();
            return false;
        }
        return encontrado;
    }

    /* Tentar selecionar o arquivo "charge_now", ou "levelFileUsing" caso contrário */
    private static boolean foundCapacity() {
        java.io.File folderFile = new java.io.File(VSet.supplyFolder);
        if (!folderFile.exists()) {
            System.out.println("Eu sei que você apagou o diretório. Vou abortar! -_-");
            System.out.println("ERRO: o diretório básico foi removido!");
            return false;
        }

        for (String file : folderFile.list()) {
            if (file.equals(VSet.CHARGE_FULL_FILENAME)) {
                // a prioridade é obter a capacidade em mAh
                /*for (String file2 : folderFile.list()) {
                    if (file2.equals(VSet.CHARGE_NOW_FILENAME)) {*/
                        return true;
                    /*}
                }*/
            } else {
                for (String file2 : folderFile.list()) {
                    if (file2.equals(VSet.CAPACITY_FILENAME)) {
                        VSet.levelFileUsing = file2;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static boolean saveSettingsFile() {
        boolean criado = true;
        VSet.firstTime = (int)(System.currentTimeMillis()/60000);
        java.io.File configFile = new java.io.File(VSet.configFolder);
        if (!configFile.exists()) {
            configFile.mkdirs();
        }

        configFile = new java.io.File(VSet.configFolder + VSet.CONFIG_FILENAME);
        try {
            configFile.delete(); // deprecated (?)
            configFile.createNewFile();
            try (java.util.Formatter f = new java.util.Formatter(configFile)) {
                f.format("%s\n%s\n%d\n\n-- Abaixo, a configuração do usuário--\n",
                        VSet.supplyFolder, VSet.levelFileUsing, VSet.firstTime);
            }
        } catch (java.io.IOException e) {
            criado = false;
        }
        return criado;
    }
}
