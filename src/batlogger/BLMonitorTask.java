package batlogger;

import java.util.logging.Level;
import java.util.logging.Logger;

/** Atualiza os dados da janela do aplicativo em segundo plano
 *
 * @author ledso
 */
public class BLMonitorTask implements Runnable {
    public enum State {
        EXIT,
        RUN,
        PAUSE
    };
    public static State state;

    @Override
    public void run() {
        state = state.RUN;
        while(state != state.EXIT) {
            if (state != state.PAUSE) {
                gui.BLHome.updateMonitor();
                gui.BLMain.updateStatus();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(BLMonitorTask.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }
    }
    
}
