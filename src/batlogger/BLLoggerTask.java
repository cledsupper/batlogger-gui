package batlogger;

import java.util.logging.Level;
import java.util.logging.Logger;

/** Atualiza o relatório e o gráfico de uso de bateria do BatLogger.
 *
 * @author Cledson Ferreira
 */
public class BLLoggerTask implements Runnable {
    public static BLMonitorTask.State state;

    @Override
    public void run() {
        state = state.RUN;
        while(state != state.EXIT) {
            if (state != state.PAUSE)
                gui.BLHome.updateLog();
            try{
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                Logger.getLogger(BLMonitorTask.class.getName()).log(Level.SEVERE, null, e);
                return;
            }
        }
    }
}
