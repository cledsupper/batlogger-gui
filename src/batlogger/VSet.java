package batlogger;

/** Deve conter endereços para arquivos do sistema e outros dados básicos.
 *
 * @author Cledson Ferreira
 */
public class VSet {
    public final static int TRIAL_DAYS_LIMIT = 2880;
    public final static String BLDAEMON_FILENAME = "batloader";
    public final static String CONFIG_FILENAME = "batlogger.conf";
    public final static String RUNNING_DAEMON_FILENAME = ".runningdaemon"; // ver cns_batloader.c
    public final static String LOG_FILENAME = "BLLog.log";
    public final static String CHARGE_NOW_FILENAME = "charge_now";
    public final static String CAPACITY_FILENAME = "capacity";
    public final static String CURRENT_FILENAME = "current_now";
    public final static String CHARGE_FULL_FILENAME = "charge_full";
    public final static String FULL_DESIGN_FILENAME = "charge_full_design";
    public final static String STATUS_FILENAME = "status";
    public final static String VOLTAGE_FILENAME = "voltage_now";
    public final static String HEALTH_FILENAME = "health";
    public final static String STATUS_CHARGING = "Carregando";
    public final static String STATUS_DISCHARGING = "Descarregando";
    public final static String STATUS_STOPPED = "Parada";
    public final static String STATUS_FULL = "Cheio";
    public final static String HEALTH_GOOD = "Boa";
    public final static String HEALTH_OLD = "É véia já";
    public final static String HEALTH_DYING = "VAI EXPLODIR!";
    public final static String HEALTH_DEAD = "ESTÁ MORTA!";
    public final static int ERRO_1_MAINDIRNOTFOUND = 1;
    public final static int ERRO_2_MAINFILENOTFOUND = 2;
    public final static int ERRO_3_OUTPUTERROR = 3;
    public final static int ERRO_4_IMAGENOTFOUND = 4;
    public final static int ERRO_5_CONFIGFILENOTFOUND = 5;

    protected static String configFolder = "USERHOME/.config/BatLogger by Ledso/";
    protected static String cacheFolder = "USERHOME/.cache/BatLogger by Ledso/";
    protected static String appFolder = "USERHOME/.batlogger/";
    protected static String supplyFolder = "/sys/class/power_supply/";
    protected static int firstTime = 0;

    /** levelFileUsing (String) é o nome do arquivo do sistema que indica o nível
  de energia da bateria.
     *  Deve ser usado com a variável supplyFolder.
     */
    protected static String levelFileUsing = CHARGE_NOW_FILENAME;
    
    public static String getAppFolder() {
        return appFolder;
    }
    public static String getConfigFolder() {
        return configFolder;
    }
    public static String getCacheFolder() {
        return cacheFolder;
    }
    public static String getSupplyFolder() {
        return supplyFolder;
    }
    public static String getLevelFileUsing() {
        return levelFileUsing;
    }
    public static int getFirstTime() {
        return firstTime;
    }
}
