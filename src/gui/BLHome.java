package gui;

import batlogger.*;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * Design melhorado da interface gráfica do BatLogger. Sempre chamada por
 * Loader.main()
 *
 * @author Cledson Ferreira
 */
public class BLHome extends javax.swing.JFrame {

    final static String ABOUT_TEXT = "<html>"
            + "<h1 style=\"text-align: center;\"><i>BatLogger GUI beta</i></h1>"
            + "<br/><span style=\"text-align: center;\">por "
            + "<a style=\"color: rgb(0,255,255);\"  href=\"https://ledsotips.blogspot.com.br/\">"
            + "Ledso</a> (c) 2018</span><br/>"
            
            + "<br/><p>BatLogger é um aplicativo escrito em C e Java que gerencia e exibe dados sobre a bateria.</p>"
            + "<p>Apesar de ser executável em outros sistemas operacionais, esta versão do BatLogger só deve funcionar"
            + " corretamente no Linux, porém toda e qualquer melhoria no código é bem vinda."
            + " Caso queira apoiar o desenvolvimento do BatLogger traduzindo ou contribuindo com código, "
            + "acesse o repositório em <a style=\"color: rgb(0,255,255);\" "
            + "href=\"https://github.com/cledsupper/batlogger-gui\">GitHub</a>.</p>"
            + "<p>Este programa utiliza algumas bibliotecas de terceiros, são elas:</p>"
            + "<ul>"
            + "<li>JFreeChart. Distribuída sob uma licença LGPL. Acesse <a style=\"color: rgb(0,255,255);\" href=\""
            + "http://www.jfree.org/jfreechart/\">http://www.jfree.org/jfreechart/</a></li>"
            + "<li>JCommon. Distribuída sob uma licença LGPL. Acesse <a style=\"color: rgb(0,255,255);\" href=\""
            + "http://www.jfree.org/jcommon/\">http://www.jfree.org/jcommon/</a></li></ul>"
            + "</html>";

    public static final String BATLOGGER_GUI_TITLE = "BatLogger GUI - ";
    public static final String TAB_MONITOR_TITLE = "Monitor";
    public static final String TAB_LOG_TITLE = "Estatísticas";
    public static final String TAB_SETTINGS_TITLE = "Configurações e ajuda";
    private static final String THIS_RESOURCE_MAY = "Este recurso pode não funcionar no seu sistema"
            + " e/ou retornar valores incorretos!";
    private static final String THIS_RESOURCE_DONT_WORKS = "Este recurso não é compatível com seu sistema!";
    private static final String WARN_TITLE = "AVISO";
    private static final String TRIAL_TITLE = "Você está usando uma versão de teste deste programa";
    private static final String TRIAL_MESSAGE = "O BatLogger GUI e seu Daemon são softwares de código aberto,"
            + " o que significa que você\n"
            + "tem os direitos de obter o código fonte desses programas, de modificar, de compilar\n"
            + ", de executar, de distribuir gratuitamente e/ou vender para outras pessoas. Leia a\n"
            + "Licença MIT para mais detalhes.\n"
            + "\nApesar disso, os arquivos executáveis deste programa NÃO são publicados gratuitamente.\n"
            + "Esta versão de testes expirará após 2 dias. Depois desse prazo, você poderá:\n"
            + " * Usar a versão básica deste programa;\n"
            + " * Baixar, modificar e compilar o código fonte para continuar usando a versão completa;\n"
            + " * Ou apoiar o desenvolvimento do projeto Upper, receber atualizações completas deste\n"
            + " ... programa e ainda obter suporte gratuito sempre que precisar.\n"
            + "\nAcesse https://ledsotips.blogspot.com.br/ para saber como :)";
    //private static final String ERROR_TITLE = "ERRO";
    private static final String DAYS_HOURS_MINUTES_SECONDS_REMAINING = "%d dias, %dh%2dm e %d segundos restantes";
    private static final String HOURS_MINUTES_SECONDS_REMAINING = "%dh%2dm e %d segundos restantes";
    private static final String MINUTES_SECONDS_REMAINING = "%d minutos e %d segundos restantes";
    private static final String SECONDS_REMAINING = " segundos restantes";
    private static final String LOG_IS_BIGGER = "O RELATÓRIO ESTÁ PESADO!";
    private static final String TAKES_MUCH_TIME = "Demorou mais de 1 segundo para o relatório ser lido."
            + " Confirmar limpeza? (\"SIM\" RECOMENDADO)";
    private static final String CLEAR_LOG_TITLE = "Limpar relatório?";
    private static final String DELETING_LOG_CAUSES = "Apagar o relatório limpará todos os dados"
            + " da aba \"%s\". Confirmar limpeza?";
    private static final String THE_DAEMON_WILL_STOP = "O daemon (serviço) "
            + "de relatório será parado após algum tempo. Continuar?";
    private static final String STOP_DAEMON = "PARAR DAEMON";
    private static final String START_DAEMON = "INICIAR DAEMON";
    private static final String DAEMON_RUNNING = "executando";
    private static final String DAEMON_STOPPED = "parado";
    private static BLHome current;
    private static Thread monitorThread;
    private static Thread loggerThread;
    //private static java.util.GregorianCalendar dischargeSessionFirstTime;
    private static long currentLastTime = 0;
    //private static double dischargeSessionFirstPercent = 100;
    private static double currentLastPercent = 100;

    // When true, the program will update the config file at the end
    private boolean hasChanges = false;
    // When true, the slowness alert will no longer be shown
    private boolean hasSlownessAlertBeenShown = false;
    // When true, the trial license alert will no longer be shown
    private boolean hasCheckedTrialPopup = false;

    /**
     * Creates new form BLHome
     */
    public BLHome() {
        initComponents();
        java.net.URL arqIcon = getClass().getResource("/media/bateria-100.png");
        java.awt.Image appIcon = java.awt.Toolkit.getDefaultToolkit().getImage(arqIcon);
        setIconImage(appIcon);
        label_baticon.setIcon(new javax.swing.ImageIcon(
                appIcon.getScaledInstance(label_baticon.getWidth(),
                        label_baticon.getHeight(), java.awt.Image.SCALE_SMOOTH)));

        //check_capacity.setSelected(false);
        check_capacityActionPerformed(null);
        //check_current.setSelected(false);
        check_currentActionPerformed(null);
        //check_health.setSelected(false);
        check_healthActionPerformed(null);
        //check_voltage.setSelected(false);
        check_voltageActionPerformed(null);
        jTextPane1.setCaretPosition(0);
        current = this;
        loadConfig();
        monitorThread = new Thread(new batlogger.BLMonitorTask());
        loggerThread = new Thread(new batlogger.BLLoggerTask());
        monitorThread.start();
        loggerThread.start();
        //BLLoggerTask.state = BLLoggerTask.STATE_PAUSED;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        espacamento_bat_top = new javax.swing.JLabel();
        espacamento_bat_left = new javax.swing.JLabel();
        espacamento_bat_right = new javax.swing.JLabel();
        batpng = new javax.swing.JLabel();
        label_status = new javax.swing.JLabel();
        label_level = new javax.swing.JLabel();
        label_currentRemainingTime = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        label_infoVoltage = new javax.swing.JLabel();
        label_voltage = new javax.swing.JLabel();
        label_infoHealth = new javax.swing.JLabel();
        label_health = new javax.swing.JLabel();
        label_infoCurrent = new javax.swing.JLabel();
        label_current = new javax.swing.JLabel();
        label_infoCapacity = new javax.swing.JLabel();
        label_capacity = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        label_useSinceLastRecharge = new javax.swing.JLabel();
        label_grafico = new javax.swing.JLabel();
        label_log = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        label_settingsTitle = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        label_about = new javax.swing.JLabel();
        check_capacity = new javax.swing.JCheckBox();
        label_monitoring = new javax.swing.JLabel();
        check_current = new javax.swing.JCheckBox();
        check_health = new javax.swing.JCheckBox();
        check_voltage = new javax.swing.JCheckBox();
        label_baticon = new javax.swing.JLabel();
        espacamento_baticon_left = new javax.swing.JLabel();
        espacamento_baticon_right = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        label_logging = new javax.swing.JLabel();
        button_deleteLog = new javax.swing.JButton();
        button_stopDaemon = new javax.swing.JButton();
        label_daemonStatusInfo = new javax.swing.JLabel();
        label_daemonStatus = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BatLogger GUI - Monitor");
        setMinimumSize(new java.awt.Dimension(698, 525));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jTabbedPane1.setFont(new java.awt.Font("Ubuntu Condensed", 3, 14)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        batpng.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        batpng.setIcon(new javax.swing.ImageIcon(getClass().getResource("/media/bateria-100.png"))); // NOI18N

        label_status.setFont(new java.awt.Font("Ubuntu Condensed", 3, 36)); // NOI18N
        label_status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_status.setText("STATUS");

        label_level.setFont(new java.awt.Font("Ubuntu Mono", 1, 96)); // NOI18N
        label_level.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_level.setText("100.0%");

        label_currentRemainingTime.setFont(new java.awt.Font("Ubuntu Condensed", 2, 18)); // NOI18N
        label_currentRemainingTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_currentRemainingTime.setText("Calculando tempo restante...");

        label_infoVoltage.setFont(new java.awt.Font("Ubuntu Condensed", 3, 12)); // NOI18N
        label_infoVoltage.setText("Tensão (V)");

        label_voltage.setFont(new java.awt.Font("Ubuntu Condensed", 1, 12)); // NOI18N
        label_voltage.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_voltage.setText("14.8 V");

        label_infoHealth.setFont(new java.awt.Font("Ubuntu Condensed", 3, 12)); // NOI18N
        label_infoHealth.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_infoHealth.setText("Saúde da bateria");

        label_health.setFont(new java.awt.Font("Ubuntu Condensed", 1, 12)); // NOI18N
        label_health.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_health.setText("Boa");

        label_infoCurrent.setFont(new java.awt.Font("Ubuntu Condensed", 3, 12)); // NOI18N
        label_infoCurrent.setText("Corrente (mA)");

        label_current.setFont(new java.awt.Font("Ubuntu Condensed", 1, 12)); // NOI18N
        label_current.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_current.setText("0 mA");

        label_infoCapacity.setFont(new java.awt.Font("Ubuntu Condensed", 3, 12)); // NOI18N
        label_infoCapacity.setText("Capacidade (mAh)");

        label_capacity.setFont(new java.awt.Font("Ubuntu Condensed", 1, 12)); // NOI18N
        label_capacity.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_capacity.setText("2000 mAh");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(espacamento_bat_left, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(batpng, javax.swing.GroupLayout.PREFERRED_SIZE, 160, Short.MAX_VALUE)
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_level, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(12, 12, 12)
                        .addComponent(espacamento_bat_right, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(37, 37, 37))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(82, 82, 82)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label_infoCapacity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label_capacity, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label_infoCurrent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label_current, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label_infoHealth, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                                    .addComponent(label_health, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(21, 21, 21)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label_infoVoltage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label_voltage, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(label_currentRemainingTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator1))
                        .addGap(31, 31, 31))))
            .addComponent(espacamento_bat_top, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(espacamento_bat_top, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(espacamento_bat_left, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_status)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_level, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_currentRemainingTime))
                    .addComponent(espacamento_bat_right, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(batpng, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_infoVoltage)
                    .addComponent(label_infoHealth)
                    .addComponent(label_infoCurrent)
                    .addComponent(label_infoCapacity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_voltage)
                    .addComponent(label_health)
                    .addComponent(label_current)
                    .addComponent(label_capacity))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Monitor", jPanel1);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(32767, 500));

        label_useSinceLastRecharge.setFont(new java.awt.Font("Noto Sans", 3, 24)); // NOI18N
        label_useSinceLastRecharge.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_useSinceLastRecharge.setText("Uso desde a última recarga");

        label_grafico.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        label_grafico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_grafico.setText("aguarde...");

        label_log.setFont(new java.awt.Font("Noto Sans", 3, 24)); // NOI18N
        label_log.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_log.setText("Relatório completo");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nível", "Tensão", "Data/hora"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Double.class, java.lang.Double.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_useSinceLastRecharge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label_log, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3)
                    .addComponent(label_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_useSinceLastRecharge, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(label_grafico, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label_log, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel4);

        jTabbedPane1.addTab("Estatísticas e relatório", jScrollPane2);

        label_settingsTitle.setFont(new java.awt.Font("Ubuntu Condensed", 2, 24)); // NOI18N
        label_settingsTitle.setText("Preferências");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        label_about.setFont(new java.awt.Font("Ubuntu Condensed", 3, 24)); // NOI18N
        label_about.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_about.setText("SOBRE");

        check_capacity.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        check_capacity.setText("Mostrar capacidade (mAh)");
        check_capacity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_capacityActionPerformed(evt);
            }
        });

        label_monitoring.setFont(new java.awt.Font("Ubuntu Condensed", 3, 18)); // NOI18N
        label_monitoring.setText("Monitoramento");

        check_current.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        check_current.setText("Mostrar corrente (mA)");
        check_current.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_currentActionPerformed(evt);
            }
        });

        check_health.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        check_health.setText("Mostrar saúde");
        check_health.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_healthActionPerformed(evt);
            }
        });

        check_voltage.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        check_voltage.setText("Mostrar tensão (Volts)");
        check_voltage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_voltageActionPerformed(evt);
            }
        });

        label_baticon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_baticon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/media/bateria-100.png"))); // NOI18N

        jTextPane1.setEditable(false);
        jTextPane1.setContentType("text/html"); // NOI18N
        jTextPane1.setFont(new java.awt.Font("Ubuntu Condensed", 0, 14)); // NOI18N
        jTextPane1.setText(ABOUT_TEXT);
        jTextPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTextPane1.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                jTextPane1HyperlinkUpdate(evt);
            }
        });
        jScrollPane1.setViewportView(jTextPane1);

        label_logging.setFont(new java.awt.Font("Ubuntu Condensed", 3, 18)); // NOI18N
        label_logging.setText("Relatório");

        button_deleteLog.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        button_deleteLog.setForeground(new java.awt.Color(255, 45, 45));
        button_deleteLog.setText("EXCLUIR TUDO");
        button_deleteLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_deleteLogActionPerformed(evt);
            }
        });

        button_stopDaemon.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        button_stopDaemon.setForeground(new java.awt.Color(255, 0, 255));
        button_stopDaemon.setText(STOP_DAEMON);
        button_stopDaemon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_stopDaemonActionPerformed(evt);
            }
        });

        label_daemonStatusInfo.setFont(new java.awt.Font("Ubuntu Condensed", 2, 14)); // NOI18N
        label_daemonStatusInfo.setText("Status do Daemon:");

        label_daemonStatus.setFont(new java.awt.Font("Ubuntu Condensed", 1, 14)); // NOI18N
        label_daemonStatus.setText("verificando...");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_settingsTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label_monitoring, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_capacity, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                    .addComponent(check_current, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_health, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_voltage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label_logging, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(label_daemonStatusInfo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(label_daemonStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(button_stopDaemon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(button_deleteLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_about, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(espacamento_baticon_left, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_baticon, javax.swing.GroupLayout.PREFERRED_SIZE, 129, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(espacamento_baticon_right, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label_about, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label_settingsTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label_baticon, javax.swing.GroupLayout.PREFERRED_SIZE, 213, Short.MAX_VALUE)
                                    .addComponent(espacamento_baticon_left, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(espacamento_baticon_right, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(label_monitoring)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(check_capacity)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(check_current)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(check_health)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(check_voltage)
                                .addGap(18, 18, 18)
                                .addComponent(label_logging)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                            .addComponent(button_deleteLog)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(button_stopDaemon))
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(label_daemonStatusInfo)
                                    .addComponent(label_daemonStatus))
                                .addGap(0, 100, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Outros", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void check_capacityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_capacityActionPerformed
        if (evt != null) {
            if (FSet.getCapacity() == -1.0) {
                javax.swing.JOptionPane.showMessageDialog(current, THIS_RESOURCE_DONT_WORKS, WARN_TITLE,
                        javax.swing.JOptionPane.INFORMATION_MESSAGE);
                check_capacity.setSelected(false);
                check_capacity.setEnabled(false);
            }
            hasChanges = true;
            updateConfig();
        }
        label_infoCapacity.setVisible(check_capacity.isSelected());
        label_capacity.setVisible(check_capacity.isSelected());
    }//GEN-LAST:event_check_capacityActionPerformed

    private void check_currentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_currentActionPerformed
        if (evt != null) {
            if (FSet.getCurrent() == -1.0) {
                javax.swing.JOptionPane.showMessageDialog(current, THIS_RESOURCE_DONT_WORKS, WARN_TITLE,
                        javax.swing.JOptionPane.INFORMATION_MESSAGE);
                check_current.setSelected(false);
                check_current.setEnabled(false);
            }
            hasChanges = true;
            updateConfig();
        }
        label_infoCurrent.setVisible(check_current.isSelected());
        label_current.setVisible(check_current.isSelected());
    }//GEN-LAST:event_check_currentActionPerformed

    private void check_healthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_healthActionPerformed
        if (evt != null) {
            if (FSet.getHealth()==VSet.HEALTH_DEAD && check_health.isSelected()) {
                javax.swing.JOptionPane.showMessageDialog(current, THIS_RESOURCE_MAY, WARN_TITLE,
                        javax.swing.JOptionPane.INFORMATION_MESSAGE);
            }
            hasChanges = true;
            updateConfig();
        }
        label_infoHealth.setVisible(check_health.isSelected());
        label_health.setVisible(check_health.isSelected());
    }//GEN-LAST:event_check_healthActionPerformed

    private void check_voltageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_voltageActionPerformed
        if (evt != null) {
            if (FSet.getVoltage() == 10.0) {
                javax.swing.JOptionPane.showMessageDialog(current, THIS_RESOURCE_DONT_WORKS, WARN_TITLE,
                        javax.swing.JOptionPane.INFORMATION_MESSAGE);
                check_voltage.setSelected(false);
                check_voltage.setEnabled(false);
            }
            hasChanges = true;
            updateConfig();
        }
        label_infoVoltage.setVisible(check_voltage.isSelected());
        label_voltage.setVisible(check_voltage.isSelected());
    }//GEN-LAST:event_check_voltageActionPerformed

    private void jTextPane1HyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {//GEN-FIRST:event_jTextPane1HyperlinkUpdate
        if (evt.getEventType() == javax.swing.event.HyperlinkEvent.EventType.ACTIVATED) {
            if (java.awt.Desktop.isDesktopSupported()) {
                try {
                    LinuxOpen.openLink(evt.getURL().toURI());
                } catch (IOException | java.net.URISyntaxException | InterruptedException e) {
                    // e.printStackTrace();
                }
            }
        }
    }//GEN-LAST:event_jTextPane1HyperlinkUpdate

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        batlogger.BLLoggerTask.state = batlogger.BLLoggerTask.state.EXIT;
        batlogger.BLMonitorTask.state = batlogger.BLMonitorTask.state.EXIT;
    }//GEN-LAST:event_formWindowClosing

    private void button_deleteLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_deleteLogActionPerformed
        if (evt != null) {
            int answer = javax.swing.JOptionPane.showConfirmDialog(current,
                    String.format(DELETING_LOG_CAUSES, current.jTabbedPane1.getTitleAt(1)), CLEAR_LOG_TITLE,
                    javax.swing.JOptionPane.YES_NO_OPTION);
            if (answer != javax.swing.JOptionPane.YES_OPTION) {
                return;
            }
        }
        File logFile = new File(VSet.getCacheFolder() + VSet.LOG_FILENAME);
        if (logFile.exists()) {
            logFile.delete();
        }
        button_deleteLog.setEnabled(false);
    }//GEN-LAST:event_button_deleteLogActionPerformed

    private void button_stopDaemonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_stopDaemonActionPerformed
        if (evt != null && button_stopDaemon.getText().equals(STOP_DAEMON)) {
            int answer = javax.swing.JOptionPane.showConfirmDialog(current, THE_DAEMON_WILL_STOP,
                    STOP_DAEMON, javax.swing.JOptionPane.YES_NO_OPTION);
            if (answer != javax.swing.JOptionPane.YES_OPTION) {
                return;
            }
        } else if (button_stopDaemon.getText().equals(START_DAEMON)) {
            try {
                Runtime.getRuntime().exec(VSet.getAppFolder() + VSet.BLDAEMON_FILENAME + " silent");
                button_stopDaemon.setText(STOP_DAEMON);
                label_daemonStatus.setText(DAEMON_RUNNING);
            } catch (IOException e) {
                javax.swing.JOptionPane.showMessageDialog(current, "Reinstale o BatLogger para corrigir isso",
                        "ERRO: BATLOADER NÃO ENCONTRADO", javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            return;
        }
        File daemonRunningFile = new File(VSet.getCacheFolder() + VSet.RUNNING_DAEMON_FILENAME);
        if (daemonRunningFile.exists()) {
            daemonRunningFile.delete();
        }
        button_stopDaemon.setText(START_DAEMON);
        label_daemonStatus.setText(DAEMON_STOPPED);
    }//GEN-LAST:event_button_stopDaemonActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        switch (jTabbedPane1.getSelectedIndex()) {
            case 0:
                setTitle(BATLOGGER_GUI_TITLE + TAB_MONITOR_TITLE);
                BLLoggerTask.state = BLMonitorTask.state.PAUSE;
                BLMonitorTask.state = BLMonitorTask.state.RUN;
                break;
            case 1:
                setTitle(BATLOGGER_GUI_TITLE + TAB_LOG_TITLE);
                if (!hasCheckedTrialPopup && !FSet.FULL_VERSION) {
                    showTrialWarn();
                    hasCheckedTrialPopup = true;
                }
                BLLoggerTask.state = BLMonitorTask.state.RUN;
                BLMonitorTask.state = BLMonitorTask.state.PAUSE;
                break;
            case 2:
                setTitle(BATLOGGER_GUI_TITLE + TAB_SETTINGS_TITLE);
                BLLoggerTask.state = BLMonitorTask.state.PAUSE;
                BLMonitorTask.state = BLMonitorTask.state.PAUSE;
        }
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        BLMonitorTask.State oldLoggerTaskState = BLLoggerTask.state;
        BLLoggerTask.state = BLLoggerTask.state.PAUSE;
        updateLog();
        BLLoggerTask.state = oldLoggerTaskState;
    }//GEN-LAST:event_formComponentResized

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the GTK+ or Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        boolean gtkLookFound = false;
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("GTK+".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    gtkLookFound = true;
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        if (!gtkLookFound) {
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(BLHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new BLHome().setVisible(true);
        });
    }

    /**
     * Atualiza as informações da aba monitor
     *
     */
    public static void updateMonitor() {
        if (current == null) {
            return;
        }
        double percent = FSet.getPercent();
        String percent_bat = "/media/bateria-erro-vazioabsoluto.png";
        if ((int) percent > 0) {
            percent_bat = "/media/bateria-005.png";
        }
        if ((int) percent > 14) {
            percent_bat = "/media/bateria-020.png";
        }
        if ((int) percent > 39) {
            percent_bat = "/media/bateria-040.png";
        }
        if ((int) percent > 59) {
            percent_bat = "/media/bateria-060.png";
        }
        if ((int) percent > 79) {
            percent_bat = "/media/bateria-080.png";
        }
        if ((int) percent > 90) {
            percent_bat = "/media/bateria-100.png";
        }
        java.awt.image.BufferedImage image = null;
        try {
            image = javax.imageio.ImageIO.read(current.getClass().getResource(percent_bat));
        } catch (IOException e) {
            System.err.println("ERRO #4: IMAGEM NÃO CARREGADA!");
            System.exit(VSet.ERRO_4_IMAGENOTFOUND);
        }
        java.awt.Image png = image.getScaledInstance(160,
                320, java.awt.Image.SCALE_SMOOTH);
        javax.swing.ImageIcon icone = new javax.swing.ImageIcon(png);
        current.batpng.setIcon(icone);

        //java.net.URL arqIcon = current.getClass().getResource(percent_bat);
        //java.awt.Image appIcon = java.awt.Toolkit.getDefaultToolkit().getImage(arqIcon);
        //current.setIconImage(icone.getImage());
        current.label_level.setText(String.format("%.1f%%", percent));
        current.label_capacity.setText(String.format("%d mAh", FSet.getChargeFull()));
        current.label_current.setText(String.format("%d mA", FSet.getCurrent()));
        current.label_voltage.setText(String.format("%.1f V", FSet.getVoltage()));
        current.label_health.setText(FSet.getHealth());
        String status = FSet.getStatus();
        current.label_status.setText(status);

        if (status == VSet.STATUS_STOPPED) {
            current.label_currentRemainingTime.setText("Não está carregando");
        } else if (status == VSet.STATUS_FULL) {
            current.label_currentRemainingTime.setText("Bateria completamente carregada");
        } else if (currentLastTime == 0) {
            currentLastTime = System.currentTimeMillis();
            currentLastPercent = percent;
        } else if (percent - currentLastPercent < -0.25) {
            java.util.Date timenow = new java.util.Date();
            java.util.Date timebefore = new java.util.Date(currentLastTime);

            double deltaT = (timenow.getTime() - timebefore.getTime()) / 1000;
            double deltaP = currentLastPercent - percent;
            double x = percent * deltaT / deltaP;
            int dias = (int) x / 86400;
            int horas = (int) (x - dias * 86400) / 3600;
            int minutos = (int) (x - horas * 3600 - dias * 86400) / 60;
            int segundos = (int) x - minutos * 60 - horas * 3600 - dias * 86400;

            if (dias > 0) {
                current.label_currentRemainingTime.setText(String.format(DAYS_HOURS_MINUTES_SECONDS_REMAINING,
                        dias, horas,
                        minutos, segundos));
            } else if (horas > 0) {
                current.label_currentRemainingTime.setText(String.format(HOURS_MINUTES_SECONDS_REMAINING,
                        horas,
                        minutos, segundos));
            } else if (minutos > 0) {
                current.label_currentRemainingTime.setText(String.format(MINUTES_SECONDS_REMAINING, minutos, segundos));
            } else {
                current.label_currentRemainingTime.setText(segundos + SECONDS_REMAINING);
            }
            current.label_status.setText(VSet.STATUS_DISCHARGING);
            currentLastTime = timenow.getTime();
            currentLastPercent = percent;
            if (percent < 15) {
                current.label_status.setText("PÕE PRA CARREGAR");
            } else if (percent < 5) {
                current.label_status.setText("VAI! MATA!!!");
            }
        } else if (percent - currentLastPercent > 0.25) {
            java.util.Date timenow = new java.util.Date();
            java.util.Date timebefore = new java.util.Date(currentLastTime);

            double deltaT = (timenow.getTime() - timebefore.getTime()) / 1000;
            double deltaP = percent - currentLastPercent;

            double x = (100 - percent) * deltaT / deltaP;
            current.label_status.setText(VSet.STATUS_CHARGING);
            int dias = (int) x / 86400;
            int horas = (int) (x - dias * 86400) / 3600;
            int minutos = (int) (x - horas * 3600 - dias * 86400) / 60;
            int segundos = (int) x - minutos * 60 - horas * 3600 - dias * 86400;
            if (dias > 0) {
                current.label_currentRemainingTime.setText(String.format(DAYS_HOURS_MINUTES_SECONDS_REMAINING,
                        dias, horas,
                        minutos, segundos));
            } else if (horas > 0) {
                current.label_currentRemainingTime.setText(String.format(HOURS_MINUTES_SECONDS_REMAINING,
                        horas,
                        minutos, segundos));
            } else if (minutos > 0) {
                current.label_currentRemainingTime.setText(String.format(MINUTES_SECONDS_REMAINING,
                        minutos, segundos));
            } else {
                current.label_currentRemainingTime.setText(segundos + SECONDS_REMAINING);
            }
            currentLastTime = timenow.getTime();
            currentLastPercent = percent;
        }
    }

    public static void updateLog() {
        int beginDischargeSessionLine = 0; // linha em que a bateria parou de carregar acima de 80%
        //int beginRechargeSessionLine = 0; // linha em que a bateria começou a recarregar
        long t_start = System.currentTimeMillis();
        File logFile = new File(VSet.getCacheFolder() + VSet.LOG_FILENAME);
        ArrayList<Object[]> logs = new ArrayList<>();
        javax.swing.table.DefaultTableModel dtm;
        Object[][] array = null;
        java.awt.image.BufferedImage chartbkg = null;
        try {
            chartbkg = javax.imageio.ImageIO.read(current.getClass().getResource("/media/chartbkg.png"));
        } catch (IOException ex) {
            Logger.getLogger(BLHome.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (new File(VSet.getCacheFolder() + VSet.RUNNING_DAEMON_FILENAME).exists()) {
            current.label_daemonStatus.setText(DAEMON_RUNNING);
            current.button_stopDaemon.setText(STOP_DAEMON);
        } else {
            current.label_daemonStatus.setText(DAEMON_STOPPED);
            current.button_stopDaemon.setText(START_DAEMON);
        }

        if (logFile.exists()) {
            try (java.util.Scanner sc = new java.util.Scanner(logFile)) {
                for (int l = 0; sc.hasNextLine(); l++) {
                    String line = sc.nextLine();
                    if (line.isEmpty()) {
                        break;
                    }
                    String[] splittedLine = new String[]{"", "", "", ""};
                    for (int cl = 0, c = 0; cl < line.length(); cl++) {
                        if (line.charAt(cl) == '|') {
                            c++;
                            cl++;
                        }
                        splittedLine[c] += line.charAt(cl);
                    }
                    Object[] splittedLineObs = new Object[]{
                        Double.parseDouble(splittedLine[0]),
                        Double.parseDouble(splittedLine[1]),
                        FSet.translateStatus(splittedLine[3]),
                        splittedLine[2]
                    };
                    logs.add(splittedLineObs);
                }
                array = new Object[logs.size()][];
                int logs_length = array.length;
                for (int l = logs_length - 1; l >= 0; l--) {
                    array[logs_length - l - 1] = logs.get(l);
                    if ((array[logs_length - l - 1][2].equals(VSet.STATUS_DISCHARGING))
                            && (Double) array[logs_length - l - 1][0] >= 90.0) {
                        beginDischargeSessionLine = l;
                    }
                }
            } catch (FileNotFoundException e) {
            }
        }
        dtm = new javax.swing.table.DefaultTableModel(array, new String[]{
            "Nível (%)",
            "Tensão (Volts)",
            "Estado",
            "Data/hora"
        });
        current.jTable1.setModel(dtm);
        long t_end = System.currentTimeMillis();
        if ((t_end - t_start) > 1000 && !current.hasSlownessAlertBeenShown) {
            int resposta = javax.swing.JOptionPane.showConfirmDialog(current,
                    TAKES_MUCH_TIME,
                    LOG_IS_BIGGER, javax.swing.JOptionPane.YES_NO_OPTION);
            if (resposta == javax.swing.JOptionPane.YES_OPTION) {
                current.button_deleteLogActionPerformed(null);
            } else current.hasSlownessAlertBeenShown = true;
        }

        // Agora vamos desenhar o gráfico :D
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        int diaAntes = 0;
        for (int l = beginDischargeSessionLine; l < logs.size(); l++) {
            Object splittedLine[] = logs.get(l);
            double nivel = (Double) splittedLine[0];
            String status = (String) splittedLine[2];
            String dataHora = (String) splittedLine[3];
            int[] tempo = new int[5]; // 0 = dia | 1 = mês | 2 = ano | 
            // 3 = hora | 4 = minuto
            for (int x = 0, c = 0; x < 5; x++) {
                if (x != 2) {
                    tempo[x] = Integer.parseInt(dataHora.subSequence(c, c + 2).toString());
                    c += 3;
                } else {
                    tempo[x] = Integer.parseInt(dataHora.subSequence(c, c + 4).toString());
                    c += 6;
                }
            }
            int diaAgora = tempo[0] + (int) (tempo[1] * 30.417 + tempo[2] * 30.417 * 12);
            String h = (diaAgora != diaAntes
                    ? String.format("dia %d", tempo[0]) : String.format("%2dh", tempo[3]));
            dcd.addValue(nivel, "Nível/hora", h);
            diaAntes = diaAgora;
        }
        JFreeChart chart = ChartFactory.createLineChart("Gráfico de uso",
                "Hora", "Nível", dcd, PlotOrientation.VERTICAL, true, true, false);
        java.awt.image.BufferedImage image = chart.createBufferedImage(
                current.label_grafico.getWidth(), current.label_grafico.getHeight());
        current.label_grafico.setText("");
        current.label_grafico.setIcon(new javax.swing.ImageIcon(image));
    }

    private static boolean updateConfig() {
        boolean success = true;

        if (!current.hasChanges) {
            return true;
        }
        current.hasChanges = false;

        System.out.println("Atualizando configuração...");
        File configFile = new File(
                VSet.getConfigFolder() + VSet.CONFIG_FILENAME);
        int firstTime = VSet.getFirstTime();
        String folder = VSet.getSupplyFolder(), capacity_filename = VSet.getLevelFileUsing();
        String capacity = (current.check_capacity.isSelected() ? "showCapacity" : "hideCapacity");
        String currentStr = (current.check_current.isSelected() ? "showCurrent" : "hideCurrent");
        String health = (current.check_health.isSelected() ? "showHealth" : "hideHealth");
        String voltage = (current.check_voltage.isSelected() ? "showVoltage" : "hideVoltage");
        try (java.util.Formatter configSaver = new java.util.Formatter(configFile)) {
            configSaver.format("%s\n"
                    + "%s\n"
                    + "%d\n\n-- AS LINHAS ANTERIORES **NUNCA** DEVEM SER TOCADAS! --\n"
                    + "%s\n"
                    + "%s\n"
                    + "%s\n"
                    + "%s\n",
                    folder, capacity_filename, firstTime, capacity, currentStr, health, voltage);
        } catch (FileNotFoundException e) {
            success = false;
        }
        return success;
    }

    private static boolean loadConfig() {
        boolean success = true;
        File configFile = new File(
                VSet.getConfigFolder() + VSet.CONFIG_FILENAME);
        try (java.util.Scanner sc = new java.util.Scanner(configFile)) {
            sc.nextLine();
            sc.nextLine();
            sc.nextLine();
            sc.nextLine();
            while (sc.hasNextLine()) {
                String setting_value = sc.nextLine();
                if (setting_value.equals("showCapacity")) {
                    current.check_capacity.setSelected(true);
                    current.check_capacityActionPerformed(null);
                }
                if (setting_value.equals("showCurrent")) {
                    current.check_current.setSelected(true);
                    current.check_currentActionPerformed(null);
                }
                if (setting_value.equals("showHealth")) {
                    current.check_health.setSelected(true);
                    current.check_healthActionPerformed(null);
                }
                if (setting_value.equals("showVoltage")) {
                    current.check_voltage.setSelected(true);
                    current.check_voltageActionPerformed(null);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("ERRO #5: arquivo de configuração não encontrado!");
            System.exit(VSet.ERRO_5_CONFIGFILENOTFOUND);
        }
        return success;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel batpng;
    private javax.swing.JButton button_deleteLog;
    private javax.swing.JButton button_stopDaemon;
    private javax.swing.JCheckBox check_capacity;
    private javax.swing.JCheckBox check_current;
    private javax.swing.JCheckBox check_health;
    private javax.swing.JCheckBox check_voltage;
    private javax.swing.JLabel espacamento_bat_left;
    private javax.swing.JLabel espacamento_bat_right;
    private javax.swing.JLabel espacamento_bat_top;
    private javax.swing.JLabel espacamento_baticon_left;
    private javax.swing.JLabel espacamento_baticon_right;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JLabel label_about;
    private javax.swing.JLabel label_baticon;
    private javax.swing.JLabel label_capacity;
    private javax.swing.JLabel label_current;
    private javax.swing.JLabel label_currentRemainingTime;
    private javax.swing.JLabel label_daemonStatus;
    private javax.swing.JLabel label_daemonStatusInfo;
    private javax.swing.JLabel label_grafico;
    private javax.swing.JLabel label_health;
    private javax.swing.JLabel label_infoCapacity;
    private javax.swing.JLabel label_infoCurrent;
    private javax.swing.JLabel label_infoHealth;
    private javax.swing.JLabel label_infoVoltage;
    private javax.swing.JLabel label_level;
    private javax.swing.JLabel label_log;
    private javax.swing.JLabel label_logging;
    private javax.swing.JLabel label_monitoring;
    private javax.swing.JLabel label_settingsTitle;
    private javax.swing.JLabel label_status;
    private javax.swing.JLabel label_useSinceLastRecharge;
    private javax.swing.JLabel label_voltage;
    // End of variables declaration//GEN-END:variables

    private void showTrialWarn() {
        JOptionPane.showMessageDialog(this, TRIAL_MESSAGE, TRIAL_TITLE, JOptionPane.YES_NO_OPTION);
    }
}
