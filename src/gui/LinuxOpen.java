package gui;

import java.io.IOException;
import java.net.URI;

/** Esta classe provém uma função para agilizar a abertura de links
 * no Linux.
 *
 * @author Cledson Ferreira
 */
public class LinuxOpen {
    public static String LINUX_LOADERS[] = {
        "xdg-open",
        "kde-open",
        "gnome-open"
        // eu não conheço os lançadores dos outros ambientes gráficos :?
    };

    public static boolean openLink(URI uri) throws IOException, InterruptedException {
        String path = uri.toString();
        Runtime runtime = Runtime.getRuntime();
        for(String loader : LINUX_LOADERS) {
            Process proc = runtime.exec(String.format("%s %s", loader, path));
            proc.waitFor();
            if (proc.exitValue()==0) return true;
        }
        return false;
    }
}
